import { OperationBancaire } from './OperationBancaire.js';
import { Journal } from './Journal.js';

export class Account {
    private _solde: number;
    private _name: string;
    private liste_operations: Array<OperationBancaire>;

    constructor(name: string, solde: number) {
        this._name = name;
        this._solde = solde;
        this.liste_operations = new Array<OperationBancaire>();
    }

    /**
     * Ajoute une opération dans sa liste d'opérations
     * puis ajoute le string de l'opération dans le journal
     */
    addOperationBancaire(operation: OperationBancaire) {
        this.liste_operations.push(operation);
        let journal = Journal.getInstance();
        journal.addOperation(operation.toString());
    }

    public getSolde(): number {
        return this._solde;
    }
    public setSolde(value: number) {
        this._solde = value;
    }
    public getName(): string {
        return this._name;
    }
    public setName(value: string) {
        this._name = value;
    }
}