export class Journal {
    private static journal: Journal;
    private _operations: Array<String>;

    private constructor() {
        this._operations = new Array<String>();
    }

    static getInstance() : Journal {
        if(!Journal.journal) {
            Journal.journal = new Journal();
        }
        return Journal.journal;
    }

    public addOperation(value: string) {
        this._operations.push(value);
        console.log(value);
    }
}