import { Account } from "./Account.js";
import { OperationBancaire, TypeOperation } from "./OperationBancaire.js";
//Comptes
const compte1 = new Account("Jean", 1500);
const compte2 = new Account("Pascal", 200);
// Opérations
const operation1 = new OperationBancaire(compte1, 100, TypeOperation.Crediter);
const operation2 = new OperationBancaire(compte2, 50, TypeOperation.Debiter);
compte1.addOperationBancaire(operation1);
compte2.addOperationBancaire(operation2);
