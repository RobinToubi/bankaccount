export class OperationBancaire {
    constructor(account, value, type) {
        this.account = account;
        this.value = value;
        this.typeOperation = type;
        if (this.typeOperation === TypeOperation.Crediter) {
            this.crediter(this.value);
        }
        else if (this.typeOperation === TypeOperation.Debiter) {
            this.debiter(this.value);
        }
    }
    crediter(value) {
        this.value = value;
        const solde = this.account.getSolde();
        this.account.setSolde(solde + value);
        this.typeOperation = TypeOperation.Crediter;
    }
    debiter(value) {
        this.value = value;
        const solde = this.account.getSolde();
        this.account.setSolde(solde - value);
        this.typeOperation = TypeOperation.Debiter;
    }
    toString() {
        const typestring = (this.typeOperation == TypeOperation.Crediter) ? "crédité" : "débité";
        return this.account.getName() + " a été " + typestring + " de " + this.value + " euros";
    }
}
export var TypeOperation;
(function (TypeOperation) {
    TypeOperation[TypeOperation["Crediter"] = 0] = "Crediter";
    TypeOperation[TypeOperation["Debiter"] = 1] = "Debiter";
})(TypeOperation || (TypeOperation = {}));
