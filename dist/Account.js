import { Journal } from './Journal.js';
export class Account {
    constructor(name, solde) {
        this._name = name;
        this._solde = solde;
        this.liste_operations = new Array();
    }
    /**
     * Ajoute une opération dans sa liste d'opérations
     * puis ajoute le string de l'opération dans le journal
     */
    addOperationBancaire(operation) {
        this.liste_operations.push(operation);
        let journal = Journal.getInstance();
        journal.addOperation(operation.toString());
    }
    getSolde() {
        return this._solde;
    }
    setSolde(value) {
        this._solde = value;
    }
    getName() {
        return this._name;
    }
    setName(value) {
        this._name = value;
    }
}
