export class Journal {
    constructor() {
        this._operations = new Array();
    }
    static getInstance() {
        if (!Journal.journal) {
            Journal.journal = new Journal();
        }
        return Journal.journal;
    }
    addOperation(value) {
        this._operations.push(value);
        console.log(value);
    }
}
