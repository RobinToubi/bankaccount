import { Account } from './Account.js';

export class OperationBancaire {
    private account: Account;
    private value: number;
    private typeOperation: TypeOperation;

    constructor(account: Account, value: number, type: TypeOperation) {
        this.account = account;
        this.value = value;
        this.typeOperation = type;

        if (this.typeOperation === TypeOperation.Crediter) {
            this.crediter(this.value);
        } else if (this.typeOperation === TypeOperation.Debiter) {
            this.debiter(this.value);
        }
    }

    private crediter(value: number) : void {
        this.value = value;
        const solde = this.account.getSolde();
        this.account.setSolde(solde + value);
        this.typeOperation = TypeOperation.Crediter;
    }

    private debiter(value: number) : void {
        this.value = value;
        const solde = this.account.getSolde();
        this.account.setSolde(solde - value);
        this.typeOperation = TypeOperation.Debiter;
    }

    toString() : string {
        const typestring = (this.typeOperation == TypeOperation.Crediter) ? "crédité" : "débité"
        return this.account.getName() + " a été " +  typestring + " de " + this.value + " euros";
    } 
}

export enum TypeOperation {
    Crediter,
    Debiter
}